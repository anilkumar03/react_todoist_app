import React, { Component } from 'react';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';

class Detailes extends Component {
    constructor(props) {
        super(props)
        this.state = {
            inputTask: '',
            tasksList: [],
            displayInput: true,
            hideCompleted: false,
            editingBox: false,
        }
    }
    componentDidMount() {
        let option1 = {
            method: 'GET',
            headers: {
                "Authorization": "Bearer a6e8f29e409451f80e6732396b4312ff978036c7"
            }
        }
        axios.get("https://api.todoist.com/rest/v1/tasks", option1)
            .then((res) => {
                this.setState({
                    tasksList: [...this.state.tasksList, res.data]
                })
            })
    }

    addingTask = () => {
        if (this.state.inputTask === '') {
            alert('You are not added any task')
        }
        let newtask = {
            "content": this.state.inputTask,
            "project_id": this.props.location.anil.data.project.id
        }
        let headers = {
            "Content-Type": "application/json",
            "X-Request-Id": this.props.location.anil.data.project.id,
            "Authorization": "Bearer a6e8f29e409451f80e6732396b4312ff978036c7"
        }
        axios.post("https://api.todoist.com/rest/v1/tasks", newtask, { headers })
            .then((res) => {
                this.setState({
                    tasksList: [...this.state.tasksList, res.data],
                })
            }).catch((error) => {
                console.log(error);
            })
        this.setState({
            inputTask: '',
            displayInput: false,
            addIcon: true
        })
    }

    activeTasks = () => {
        let result = this.state.tasksList.filter(
            (task) =>
            (task.project_id === this.props.location.anil.data.project.id &&
                task.completed === false))
        if (result) {
            return result
        }
        else {
            return []
        }
    }

    completedTasks = () => {
        let filteredTask = this.state.tasksList.filter(
            (task) =>
                task.project_id === this.props.location.anil.data.project.id &&
                task.completed !== false
        );
        if (filteredTask) {
            return filteredTask;
        } else {
            return [];
        }
    }

    hideCompetedTasks = () => {
        this.setState({
            hideCompleted: !this.state.hideCompleted
        })
    }

    handleCheckBox = (completedId) => {
        let requiredTask = this.state.tasksList.find((task) => (task.id === completedId))
        let url = ''
        if (requiredTask.completed) {
            url = `https://api.todoist.com/rest/v1/tasks/${completedId}/reopen`
        }
        else {
            url = `https://api.todoist.com/rest/v1/tasks/${completedId}/close`
        }
        const headers = {
            "Authorization": "Bearer a6e8f29e409451f80e6732396b4312ff978036c7"
        }
        axios.post(url, null, { headers })
            .then((res) => {
                this.setState({
                    tasksList: this.state.tasksList.map((each) => {
                        if (each.id === completedId) {
                            return {
                                ...each,
                                completed: !each.completed,
                            };
                        }
                        return each;
                    }),
                });
            })
            .catch((error) => {
                console.log(error)
            });
    }

    deleteTask = (taskId) => {
        const headers = {
            "Authorization": "Bearer a6e8f29e409451f80e6732396b4312ff978036c7"
        }
        axios.delete(`https://api.todoist.com/rest/v1/tasks/${taskId}`, { headers })

        let remainingTasks = this.state.tasksList.filter((task) => (task.id !== taskId))
        this.setState({
            tasksList: remainingTasks
        })
    }

    inputTask = (e) => {
        this.setState({
            inputTask: e.target.value,
        })
    }

    

    addTaskContent = () => {
        this.setState({
            displayInput: false,
        })
    }

    disableInputTab = () => {
        this.setState({
            displayInput: true,
        })
    }

    render() {
        const filteredTasks = this.activeTasks()
        const completedTasks = this.completedTasks()
        const hideBtn = this.state.hideCompleted ? 'Show completed' : 'Hide completed'
        const hide_show = this.state.hideCompleted ? 'To show completed': 'To hide completed'
        return (
            <div className='container'>
                <div className='row'>
                    <section className='each-project'>
                        <div className='text-align-center'>
                            <div className='title d-flex justify-content-between'>
                                <h6 className='fw-bold fs-4'>{this.props.location.anil.data.project.name}</h6>

                                <div className='d-flex align-items-center'>
                                    <div className='d-flex text-secondary'>
                                        {/* <p onClick={this.toComment} className='p-1 opt'>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" fill="currentColor" className="bi bi-chat-square text-secondary m-1" viewBox="0 0 16 16">
                                                <path d="M14 1a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1h-2.5a2 2 0 0 0-1.6.8L8 14.333 6.1 11.8a2 2 0 0 0-1.6-.8H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h2.5a1 1 0 0 1 .8.4l1.9 2.533a1 1 0 0 0 1.6 0l1.9-2.533a1 1 0 0 1 .8-.4H14a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
                                            </svg>
                                            <span className='m-1'>comment</span>
                                        </p> */}

                                        {/* <p className='p-1 opt'>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" fill="currentColor" className="bi bi-person-plus m-1" viewBox="0 0 16 16">
                                                <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
                                                <path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z" />
                                            </svg>
                                            <span className='m-1'>share</span>
                                        </p> */}

                                        <p className='p-1 opt'>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M15 14.5a2 2 0 011.936 1.498L19.5 16a.5.5 0 010 1l-2.563.001a2.001 2.001 0 01-3.874 0L4.5 17a.5.5 0 010-1l8.564-.002A2 2 0 0115 14.5zm-.982 1.81l.005-.025-.005.026-.003.014-.004.025-.007.061A.897.897 0 0014 
                                                                16.5l.008.125.007.047-.001.002.003.014.006.024h-.001l.004.018.016.058.007.021.004.013.009.026.013.033.012.027-.011-.026.019.043-.008-.017.029.06-.018-.037.048.09a1 1 0 001.784-.155l.015-.039.006-.018-.015.039.022-.06-.001-.001.016-.057.004-.018.005-.024.001-.006v-.001l.005-.033.008-.06A.877.877 0 0016 
                                                                16.5l-.008-.124-.007-.051-.001-.001-.003-.014-.01-.047-.004-.016-.007-.024-.01-.034-.004-.012-.01-.03-.006-.013-.007-.017-.01-.026a.998.998 0 00-1.843.043l-.014.034-.007.022-.014.047-.002.009v.001l-.005.016-.01.047zM9 9.5a2 2 0 011.936 1.498L19.5 11a.5.5 0 010 1l-8.563.001a2.001 2.001 0 01-3.874 0L4.5 
                                                                12a.5.5 0 010-1l2.564-.002A2 2 0 019 9.5zm0 1a.998.998 0 00-.93.634l-.014.034-.007.022-.014.047-.002.009v.001l-.005.016-.01.047.005-.025-.005.026-.003.014-.004.025-.007.061C8 11.441 8 11.471 8 11.5l.008.125.007.047-.001.002.003.014.006.024h-.001l.004.018.016.058.007.021.004.013.009.026.013.033.012.027-.011-.026.019.043-.008-.017.029.06-.018-.037.048.09a1
                                                                1 0 001.784-.155l.015-.039.006-.018-.015.039.022-.06-.001-.001.016-.057.004-.018.005-.024.001-.006v-.001l.005-.033.008-.06A.877.877 0 0010 11.5l-.008-.124-.007-.051-.001-.001-.003-.014-.01-.047-.004-.016-.007-.024-.01-.034-.004-.012-.01-.03-.006-.013-.007-.017-.01-.026A1.002 1.002 0 009 10.5zm6-6a2 2 0 011.936 1.498L19.5 6a.5.5 0 010 1l-2.563.001a2.001 
                                                                2.001 0 01-3.874 0L4.5 7a.5.5 0 010-1l8.564-.002A2 2 0 0115 4.5zm0 1a.998.998 0 00-.93.634l-.014.034-.007.022-.014.047-.002.009v.001l-.005.016-.01.047.005-.025-.005.026-.003.014-.004.025-.007.061C14 6.441 14 6.471 14 6.5l.008.125.007.047-.001.002.003.014.006.024h-.001l.004.018.016.058.007.021.004.013.009.026.013.033.012.027-.011-.026.019.043-.008-.017.029.06-.018-.037.048.09a1 
                                                                1 0 001.784-.155l.015-.039.006-.018-.015.039.022-.06-.001-.001.016-.057.004-.018.005-.024.001-.006v-.001l.005-.033.008-.06C16 6.557 16 6.528 16 6.5l-.008-.124-.007-.051-.001-.001-.003-.014-.01-.047-.004-.016-.007-.024-.01-.034-.004-.012-.01-.03-.006-.013-.007-.017-.01-.026A1.002 1.002 0 0015 5.5z" fill="currentColor" fill-rule="nonzero"
                                            ></path>
                                            </svg>
                                            <span className=''>view</span>
                                        </p>
                                    </div>
                    {/* Icons part */}
                                    <div title = {`${hide_show}`} className="btn-group otp">
                                        <p className="btn btn-sm fs-3" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23" height="26"><g fill="none" stroke="currentColor"
                                                stroke-linecap="round" transform="translate(3 10)"><circle cx="2" cy="2" r="2"></circle><circle cx="9"
                                                    cy="2" r="2"></circle><circle cx="16" cy="2" r="2"></circle></g>
                                            </svg>
                                        </p>
                                        <ul className="dropdown-menu">
                                            <p onClick={this.hideCompetedTasks} type='button' className='btn'>{hideBtn}</p>
                                        </ul>
                                    </div>
                                </div>
                            </div>

    {/* Displaying-part */}     <div>
                                {
                                    filteredTasks.map((task) => {
                                        return (
                                            <div key={uuidv4()} className='d-flex justify-content-between'>
                                                <div id={this.props.location.anil.data.project.id} className='d-flex align-items-center p-2'>
                                                    <input
                                                        checked={task.completed}
                                                        type='checkbox'
                                                        className="check-round"
                                                        onChange={() => this.handleCheckBox(task.id)}
                                                        id={this.props.location.anil.data.project.id}
                                                    />
                                                    <label htmlFor={this.props.location.anil.data.project.id} className={this.state.checkStatus ? "strike" : null} style={{ marginLeft: "2px" }}>
                                                        {task.content}
                                                    </label>
                                                </div>

                                                <div className="btn-group dropend">

                                                    <p type="button" className="btn fs-4" data-bs-toggle="dropdown" aria-expanded="false">
                                                        <svg xmlns="http://www.w3.org/2000/svg" className='opt text-secondary' width="23" height="18"><g fill="none" stroke="currentColor"
                                                            stroke-linecap="round" transform="translate(3 10)"><circle cx="2" cy="2" r="2"></circle><circle cx="9"
                                                                cy="2" r="2"></circle><circle cx="16" cy="2" r="2"></circle></g>
                                                        </svg>
                                                    </p>
                                                    <ul className="dropdown-menu">
                                                        {/* <li onClick={() => { this.editTheTask(task.id) }} id={this.props.location.anil.data.project.id} className="dropdown-item">Edit task</li> */}
                                                        <li onClick={() => { this.deleteTask(task.id) }} id={this.props.location.anil.data.project.id} className="dropdown-item">Delete task</li>
                                                    </ul>

                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                            {/* Adding New Task */}
                            {this.state.displayInput
                                ?
                                <div style={{ display: "none" }} onClick={this.addTaskContent} type='button' className='d-flex align-items-center'>
                                    <p className='text-danger fs-5 m-1'>+</p>
                                    <p className='text-secondary m-1'>Add</p>
                                </div>
                                :
                                <div className='d-flex flex-column'>
                                    <textarea onChange={this.inputTask} value={this.state.inputTask} placeholder="Description" rows="5" className='adding-task'></textarea>
                                    <div>
                                        <button onClick={this.addingTask} className='btn btn-danger m-1'>Add task</button>
                                        <button onClick={this.disableInputTab} className='btn btn-light text-dark m-1'>Cancel</button>
                                    </div>
                                </div>
                            }


{/* Completed Task*/}
                            {this.state.hideCompleted ? null :
                                completedTasks.map((task) => {
                                    return (
                                        <div key={uuidv4()} className='d-flex justify-content-between'>
                                            <div id={this.props.location.anil.data.project.id} className='d-flex align-items-center p-2'>
                                                <input
                                                    checked={task.completed}
                                                    type='checkbox'
                                                    className="check-round"
                                                    onChange={() => this.handleCheckBox(task.id)}
                                                    id={this.props.location.anil.data.project.id}
                                                />
                                                <label htmlFor={this.props.location.anil.data.project.id} className={task.completed ? "strike" : null} style={{ marginLeft: "2px" }}>
                                                    {task.content}
                                                </label>
                                            </div>
                                            <div className="btn-group dropend">
                                                <p type="button" className="btn fs-4" data-bs-toggle="dropdown" aria-expanded="false">
                                                    ...
                                                </p>
                                                <ul className="dropdown-menu">
                                                    <li onClick={() => { this.deleteTask(task.id) }} id={this.props.location.anil.data.project.id} className="dropdown-item">Delete task</li>
                                                </ul>
                                            </div>
                                            <hr className='m-1' />
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </section>
                </div>
            </div>

        );
    }
}

export default Detailes;