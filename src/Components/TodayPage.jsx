import React, { Component } from 'react';
//import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';

class Today extends Component {

    constructor(props) {
        super(props)
        this.state = {
            inputTask: {
                content: '',
                id: 0
            },
            displayInput: true,
            todayTasks: [],
        }
    }



    changeCheckbox = (e) => {
        let requiedTask = e.target.id
        let filtered = this.state.todayTasks.filter((task) => (task.id !== requiedTask))
        this.setState({
            todayTasks: filtered
        })
    }

    addingTask = () => {
        if (this.state.inputTask === '') {
            alert('Please keep a valid task')
        }
        else {
            let newTask = this.state.inputTask
            this.setState({
                todayTasks: [...this.state.todayTasks, newTask],
                inputTask: {
                    content: ''
                }
            })
        }

    }

    inputTask = (e) => {
        this.setState({
            inputTask: {
                content: e.target.value,
                id: uuidv4()
            }
        })
    }

    addTaskContent = () => {
        this.setState({
            displayInput: false,
        })
    }

    disableInputTab = () => {
        this.setState({
            displayInput: true,
        })
    }

    render() {
        let d = new Date()
        var monthsArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var weekArray = ['Sun', "Mon", 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
        let week = weekArray[d.getDay()]
        let date = d.getDate()
        let month = monthsArray[d.getMonth()]
        return (
            <div className='d-flex flex-column text-align-left todayPage' style={{ width: '50%' }}>
                <div className='d-flex justify-content-between'>
                    <p className='fw-bolder fs-4 m-0'>Today <span className='fs-7 fw-light text-secondary'>{week} {date} {month} </span></p>
                    {/* Icons part */}
                </div>
                {
                    this.state.todayTasks.map((task, id) => {
                        return (
                            <div key={uuidv4()} className='d-flex justify-content-between'>
                                <li id={id} className='d-flex align-items-center p-2'>
                                    <input
                                        onClick={this.changeCheckbox}
                                        type='checkbox'
                                        className="check-round"
                                        id={task.id}
                                    />
                                    <label htmlFor={task.id} style={{ marginLeft: "2px" }}>
                                        {task.content}
                                    </label>
                                </li>
                            </div>
                        )
                    })

                }
                {this.state.displayInput
                    ?
                    <div style={{ display: "none" }} onClick={this.addTaskContent} type='button' className='d-flex align-items-center'>
                        <p className='text-danger fs-5 m-1'>+</p>
                        <p className='text-secondary m-1'>Add</p>
                        <div>

                        </div>
                    </div>
                    :
                    <div className='d-flex flex-column'>
                        <textarea onChange={this.inputTask} value={this.state.inputTask.content} placeholder="Task name" rows="5" className='adding-task'>         
                        </textarea>
                        <div>
                            <button onClick={this.addingTask} className='btn btn-danger m-1'>Add task</button>
                            <button onClick={this.disableInputTab} className='btn btn-light text-dark m-1'>Cancel</button>
                        </div>
                    </div>
                }
                {
                    
                }
            </div>
        );
    }
}

export default Today;