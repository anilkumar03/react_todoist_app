import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import { Modal, Button, } from 'react-bootstrap';

class SideBar extends Component {
    state = {
        myProjects: [],
        givenInput: '',
        colors: { 30: "#b8256f", 31: "#db4035", 32: "#ff9933", 36: "#299438", 47: "#808080", 48: "#b8b8b8" },
        colorCode: '',
        favoriteProjects: [],
        show: false,
    }
    handleClose = () => {
        this.setState({
            show: false
        })
    }
    handleShow = () => {
        this.setState({
            show: true
        })
    }

    componentDidMount() {
        let option1 = {
            method: 'GET',
            headers: {
                "Authorization": "Bearer a6e8f29e409451f80e6732396b4312ff978036c7"
            }
        }
        axios.get("https://api.todoist.com/rest/v1/projects", option1)
            .then((res) => {
                this.setState({
                    myProjects: res.data
                })
            }).catch((error) => {
                console.log(error);
            });
    }

    userInput = (e) => {
        this.setState({
            givenInput: e.target.value
        })

    }

    addToFavorite = (projectId) => {

    }

    addProject = (e) => {
        if (this.state.givenInput === '') {
            alert('Please enter the project')
        }
        else {
            let projectName = { "name": this.state.givenInput, "color": this.state.colorCode };
            let headers = {
                "Content-Type": "application/json",
                "X-Request-Id": uuidv4(),
                "Authorization": "Bearer a6e8f29e409451f80e6732396b4312ff978036c7"
            }
            axios.post("https://api.todoist.com/rest/v1/projects", projectName, { headers })
                .then((res) => {
                    res.data.color = this.state.colorCode
                    this.setState({
                        myProjects: [...this.state.myProjects, res.data]
                    })
                }).catch((error) => {
                    console.log(error);
                })
            this.setState({
                givenInput: ''
            })
        }
    }

    editProject = (projectId) => {
        let name = this.state.givenInput
        let newName = { "name": name }
        let headers = {
            "Content-Type": "application/json",
            "X-Request-Id": uuidv4(),
            "Authorization": "Bearer a6e8f29e409451f80e6732396b4312ff978036c7"
        }
        axios.post(`https://api.todoist.com/rest/v1/projects/${projectId}`, newName, { headers })
            .catch((error) => {
                console.log(error);
            })
        let edited = this.state.myProjects.map((each) => {
            if (each.id === projectId) {
                each.name = name
                return each
            }
            return each
        })
        console.log(edited);
        console.log(this.state.myProjects);
        this.setState({
            myProjects: edited
        })
    }

    colourNotation = (e) => {
        let c = e.target.value
        this.setState({
            colorCode: c
        })
    }

    deleteProject = (projectId) => {
        let headers = {
            "Authorization": "Bearer a6e8f29e409451f80e6732396b4312ff978036c7"
        }
        axios.delete(`https://api.todoist.com/rest/v1/projects/${projectId}`, { headers })
            .then((res) => {
                let remainProjects = this.state.myProjects.filter((each) => (each.id !== projectId))
                this.setState({
                    myProjects: remainProjects
                })
            })
    }

    render() {
        const { myProjects } = this.state

        return (
            <>
                <div className='bg-light side-bar bg-light d-flex justify-content-start'>
                    <div className='d-flex flex-column'>
                        {/* Today */}
                        <div className='d-flex align-items-center opt'>
                            <svg width="24" height="22" viewBox="0 0 24 24" className="text-success ONBJEQtK++jnfUWJ3V90Dw=="><g fill="currentColor" fill-rule="evenodd"><path fill-rule="nonzero" d="M6 4.5h12A1.5 1.5 0 0 1 19.5 6v2.5h-15V6A1.5 1.5 0 0 1 6 4.5z" opacity=".1"></path><path fill-rule="nonzero" d="M6 4h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2zm0 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V6a1 1 0 0 0-1-1H6zm1 3h10a.5.5 0 1 1 0 1H7a.5.5 0 0 1 0-1z"></path><text font-family="-apple-system, system-ui, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'" font-size="1" transform="translate(4 2)" font-weight="500"><tspan x="8" y="15" text-anchor="middle">21</tspan></text></g></svg>
                            <Link to='/today' className='p-1 text-dark fw-light'>Today</Link>
                        </div>

                        {/* projects Accordian */}
                        <div className="accordion accordion-flush" id="accordionFlushExample1">
                            <div className="accordion-item project">
                                <h2 className="accordion-header d-flex align-items-center bg-light" id="flush-headingOne1">
                                    <Link to='/projects'>
                                        <div className="accordion-button collapsed bg-light text-dark fw-bolder fs-6 p-1" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne1" aria-expanded="false" aria-controls="flush-collapseOne1">
                                            Projects
                                        </div>
                                    </Link>
                                    <div >
                                        <p type="button" className="btn fs-4 m-1" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            +
                                        </p>

                                        {/* Creating New project modal */}    <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div className="modal-dialog">
                                                <div className="modal-content p-2">
                                                    <div className="modal-header">
                                                        <h5 className="modal-title" id="exampleModalLabel">Add project</h5>
                                                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div className="modal-body">
                                                        <p className='m-1'>Name</p>
                                                        <input type="text"
                                                            className='adding'
                                                            onChange={this.userInput}
                                                            value={this.state.givenInput}
                                                        />
                                                        <hr className='mt-3 mb-3' />

                            {/* Selecting color */}
                                                        <p className='m-1'>Colour</p>
                                                        <select onClick={this.colourNotation} id="" className="adding-colour">
                                                            <option className='fw-bold' style={{ color: "gray" }} value="47">Charcoal</option>
                                                            <option className='fw-bold' style={{ color: "rgb(224, 143, 143)" }} value="30">Berry Red</option>
                                                            <option className='fw-bold' style={{ color: "red" }} value="31">Red</option>
                                                            <option className='fw-bold' style={{ color: "green" }} value="36">Green</option>
                                                            <option className='fw-bold' style={{ color: "orange" }} value="32">Orange</option>
                                                            <option className='fw-bold' style={{ color: "grey" }} value="48">Grey</option>
                                                        </select>

                                                    </div>
                                                    <div className="modal-footer">
                                                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                                        <button onClick={this.addProject} type="button" data-bs-dismiss="modal" className="btn btn-primary">Add</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </h2>
                                <div id="flush-collapseOne1" className="accordion-collapse collapse bg-light" aria-labelledby="flush-headingOne1" data-bs-parent="#accordionFlushExample1">
                                    <div className="accordion-body">
                                        {
                                            myProjects.filter(p => (p.name !== 'Inbox')).map((project) => {
                                                return (
                                                    <div key={project.id} className='d-flex justify-content-between align-items-center'>
                                                        <Link to={{ pathname: `/project/${project.id}`, anil: { data: { project } } }}>
                                                            <p id={project.id} className='text-dark'><span style={{ color: `${this.state.colors[project.color]}` }}>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" fill={`${this.state.colors[project.color]}`} className="bi bi-circle-fill m-2" viewBox="0 0 16 16">
                                                                    <circle cx="8" cy="8" r="8" />
                                                                </svg>
                                                            </span>
                                                                {project.name}</p>
                                                        </Link>
                                                        <div className="btn-group dropend opt">
                                                            <p type="button" className="btn btn-light" data-bs-toggle="dropdown" aria-expanded="false">
                                                                ...
                                                            </p>
                                                            <ul className="dropdown-menu border bg-none">

                    {/* Edit Project  */}
                                                                <Button id={uuidv4()} variant="light" className="dropdown-item" onClick={this.handleShow}>
                                                                    Edit Project
                                                                </Button>

                                                                <Modal show={this.state.show} onHide={this.handleClose}>
                                                                    <Modal.Header closeButton>
                                                                        <Modal.Title>Edit your Project</Modal.Title>
                                                                    </Modal.Header>
                                                                    <Modal.Body>
                                                                        <p className='m-1'>Name</p>
                                                                        <input type="text"
                                                                            className='adding'
                                                                            onChange={this.userInput}
                                                                            value={this.state.givenInput}
                                                                        />
                                                                        <hr className='mt-3 mb-3' />

                         {/* Selecting color */}
                                                                        <p className='m-1'>Colour</p>
                                                                        <select onClick={this.colourNotation} id="" className="adding-colour">
                                                                            <option className='fw-bold' style={{ color: "gray" }} value="47">Charcoal</option>
                                                                            <option className='fw-bold' style={{ color: "rgb(224, 143, 143)" }} value="30">Berry Red</option>
                                                                            <option className='fw-bold' style={{ color: "red" }} value="31">Red</option>
                                                                            <option className='fw-bold' style={{ color: "green" }} value="36">Green</option>
                                                                            <option className='fw-bold' style={{ color: "orange" }} value="32">Orange</option>
                                                                            <option className='fw-bold' style={{ color: "grey" }} value="48">Grey</option>
                                                                        </select>
                                                                    </Modal.Body>
                                                                    <Modal.Footer>
                                                                        <Button variant="secondary" onClick={this.handleClose}>
                                                                            Close
                                                                        </Button>
                                                                        <Button variant="primary" onClick={() => this.editProject(project.id)}>
                                                                            Save Changes
                                                                        </Button>
                                                                    </Modal.Footer>
                                                                </Modal>

                                                                <li id={uuidv4()} onClick={() => this.addToFavorite(project.id)} className="dropdown-item">Favorite project</li>
                                                                <li id={uuidv4()} onClick={() => this.deleteProject(project.id)} className="dropdown-item">Delete project</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default SideBar;