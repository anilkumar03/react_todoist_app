import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import './App.css';
import Header from './Components/Header';
import SideBar from './Components/SideBar';
import Today from './Components/TodayPage';
import Detailes from './Components/Detailes';

class App extends Component {
  state = {}
  render() {
    return (
      <BrowserRouter>
        <Header />
        <section className='d-flex'>
          <SideBar/>
          <Switch>
            <Route exact path="/project/:id" component={Detailes} />
            <Route exact path="/today" component = {Today}/>
          </Switch>
        </section>
      </BrowserRouter>
    );
  }
}

export default App;